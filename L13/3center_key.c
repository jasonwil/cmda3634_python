#include <stdio.h>
#include <math.h>
#include <float.h>

float distance_sq(float* p1, float* p2) {
    return ((p1[0]-p2[0])*(p1[0]-p2[0])+(p1[1]-p2[1])*(p1[1]-p2[1]));
}

float min3 (float a, float b, float c) {
    float min = a;
    if (b < min) {
        min = b;
    }
    if (c < min) {
        min = c;
    }
    return min;
}

float centers_cost(int num, float* pts, int c1, int c2, int c3) {
    float cost_sq = 0;
    for (int i=0;i<num;i++) {
        float dist_sq_1 = distance_sq(pts+2*i,pts+2*c1);
        float dist_sq_2 = distance_sq(pts+2*i,pts+2*c2);
        float dist_sq_3 = distance_sq(pts+2*i,pts+2*c3);
        float min_dist_sq = min3(dist_sq_1,dist_sq_2,dist_sq_3);
        if (min_dist_sq > cost_sq) {
            cost_sq = min_dist_sq;
        }
    }
    return (sqrt(cost_sq));
}

void solve_3center (int num, float* pts, int* centers) {
    int num_checked = 0;
    float min_cost = FLT_MAX;
    for (int i=0;i<num-2;i++) {
        for (int j=i+1;j<num-1;j++) {
            for (int k=j+1;k<num;k++) {
                num_checked += 1;
                float cost = centers_cost(num,pts,i,j,k);
                if (cost < min_cost) {
                    min_cost = cost;
                    centers[0] = i;
                    centers[1] = j;
                    centers[2] = k;
                }
            }
        }
    }
    printf ("number of triples checked = %d\n",num_checked);
}
