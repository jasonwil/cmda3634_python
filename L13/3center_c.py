import sys
import numpy as np
import matplotlib.pyplot as plt

# Load our C function for solving the 3 center problem
from ctypes import c_int, c_void_p, cdll
lib = cdll.LoadLibrary("./3center.so")
solve_3center_c = lib.solve_3center

# read the cities data file
cities = np.loadtxt(sys.stdin,dtype=np.float32)

# compute the distance squared between two points
def distance_sq(p1,p2):
    return (p1[0]-p2[0])*(p1[0]-p2[0])+(p1[1]-p2[1])*(p1[1]-p2[1])

# return the minimum of the 3 input values
def min3(a,b,c):
    min = a
    if (b < min):
        min = b
    if (c < min):
        min = c
    return min

# compute the cost given three centers
def centers_cost(pts,c1,c2,c3):
    cost_sq = 0
    num = len(pts)
    for i in range(num):
        dist_sq_1 = distance_sq(pts[i],pts[c1])
        dist_sq_2 = distance_sq(pts[i],pts[c2])
        dist_sq_3 = distance_sq(pts[i],pts[c3])
        min_dist_sq = min3(dist_sq_1,dist_sq_2,dist_sq_3)
        if (min_dist_sq > cost_sq):
            cost_sq = min_dist_sq
    return np.sqrt(cost_sq)

# solve the 3 center problem
def solve_3center(pts,centers):
    num_checked = 0
    min_cost = float("inf")
    num = len(pts)
    for i in range(0,num-2):
        for j in range(i+1,num-1):
            for k in range(j+1,num):
                num_checked += 1
                cost = centers_cost(pts,i,j,k)
                if (cost<min_cost):
                    min_cost = cost
                    centers[0] = i
                    centers[1] = j
                    centers[2] = k
    print ('number of triples checked =',num_checked)

# Solve the 3 center problem using our C function
centers = np.zeros(3,dtype='int32')
#solve_3center(cities,centers)
solve_3center_c(c_int(len(cities)),
        c_void_p(cities.ctypes.data),
        c_void_p(centers.ctypes.data))
c1 = centers[0]
c2 = centers[1]
c3 = centers[2]
print ('3center solution is c1 =',c1,', c2 =',c2,', c3 =',c3)

# compute the cost, clusters, and extreme point given three centers
def centers_cost_plus(pts,c1,c2,c3,clusters):
    cost_sq = 0
    num = len(pts)
    for i in range(num):
        dist_sq_1 = distance_sq(pts[i],pts[c1])
        dist_sq_2 = distance_sq(pts[i],pts[c2])
        dist_sq_3 = distance_sq(pts[i],pts[c3])
        clusters[i] = c1
        min_dist_sq = dist_sq_1
        if (dist_sq_2 < min_dist_sq):
            clusters[i] = c2
            min_dist_sq = dist_sq_2
        if (dist_sq_3 < min_dist_sq):
            clusters[i] = c3
            min_dist_sq = dist_sq_3
        if (min_dist_sq > cost_sq):
            cost_sq = min_dist_sq
            extreme = i
    return (np.sqrt(cost_sq),extreme)

# print the cost of the optimal solution
clusters = np.zeros(len(cities),dtype='int32')
cost, extreme = centers_cost_plus(cities,c1,c2,c3,clusters)
print ('cost for c1 =',c1,', c2 =',c2,', c3 =',c3,'is',cost)

# plot the cities showing centers, clusters, and the extreme point
plt.gca().set_aspect('equal')
plt.scatter(cities[clusters==c1,0],cities[clusters==c1,1],s=10,color='orange')
plt.scatter(cities[clusters==c2,0],cities[clusters==c2,1],s=10,color='maroon')
plt.scatter(cities[clusters==c3,0],cities[clusters==c3,1],s=10,color='teal')
plt.scatter(cities[c1,0],cities[c1,1],s=100,color='orange')
plt.scatter(cities[c2,0],cities[c2,1],s=100,color='maroon')
plt.scatter(cities[c3,0],cities[c3,1],s=100,color='teal')
plt.scatter(cities[extreme,0],cities[extreme,1],
        s=50,facecolors='none', edgecolors='black')
plt.savefig("3center.png")

